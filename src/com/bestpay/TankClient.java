package com.bestpay;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * User: thirdparty
 * Date: 14-10-1
 * Time: 下午11:28
 * To change this template use File | Settings | File Templates.
 */
public class TankClient extends Frame {


    public static final int GAME_WIDTH = 800;
    public static final int GAME_HEIGHT = 600;
    int x, y = 50;

    Image offScreenImage = null;

    @Override
    public void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.RED);
        g.fillOval(x, y, 30, 30);
        g.setColor(c);

        //y += 5;
    }


    @Override
    public void update(Graphics g) {
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        Graphics graphics = offScreenImage.getGraphics();
        Color c = graphics.getColor();
        graphics.setColor(Color.GREEN);
        graphics.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        graphics.setColor(c);
        paint(graphics);
        g.drawImage(offScreenImage, 0, 0, null);
    }


    public void lanchFrame() {
        this.setLocation(400, 300);
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setTitle("this show ThankWar");
        this.addWindowListener(new WindowAdapter() {
            /**
             * Invoked when a window is in the process of being closed.
             * The close operation can be overridden at this point.
             */
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        this.setResizable(false);
        this.setVisible(true);
        this.setBackground(Color.GREEN);

        // 坦克按键控制添加
        this.addKeyListener(new KeyMoniter());

        new Thread(new PaintThead()).start();
        //new PaintThead().start();
    }

    // print game window the The war
    public static void main(String[] args) {
        TankClient t = new TankClient();
        t.lanchFrame();
    }

    private class PaintThead extends Thread {

        @Override
        public void run() {
            while (true) {
                repaint();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    // Key监听器
    private class KeyMoniter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            switch (key) {
                case KeyEvent.VK_LEFT:
                    x -= 5;
                    break;
                case KeyEvent.VK_UP:
                    y -= 5;
                    break;
                case KeyEvent.VK_DOWN:
                    y += 5;
                    break;
                case KeyEvent.VK_RIGHT:
                    x += 5;
                    break;
            }
        }
    }
}

